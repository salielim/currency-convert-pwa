import React, { Component } from 'react';
import './App.css';
import Select from 'react-select';
import countriesData from './countriesData';
import axios from 'axios';

const CurrContext = React.createContext();

class CountriesList extends React.Component {
  state = {
    fromCurr: null,
    toCurr: null,
    result: null
  };

  async getRateApi() {
    try {
      if (this.state.fromCurr !== null && this.state.toCurr !== null) {
        console.log('in if');
        
        const response = await axios.get(`https://free.currencyconverterapi.com/api/v6/convert?q=
          ${this.state.fromCurr.value}_${this.state.toCurr.value}
        &compact=ultra`);
        const result = response.data[Object.keys(response.data)[0]] * 1;

        this.setState({
          result: result
        });
        console.log(result);
        return result
      }
    } catch (err) {
      console.log(err);
    }
  }

  selectHandler = selectedOption => {
    let curr = (this.props.name === 'fromCurr') ? 'fromCurr' : 'toCurr';
    this.setState({ [curr]: selectedOption }, () => {
      console.log(this.state);
    })
    this.getRateApi();
  };

  render() {
    // label and value for dropdown list
    const countriesArr = Object.values(countriesData);
    let countriesOptions = [];
    for (var i = 0; i < countriesArr.length; i++) {
      countriesOptions.push({
        value: `${countriesArr[i].id}`,
        label: `${countriesArr[i].currencyName} (${countriesArr[i].id})`
      });
    }

    return (
      <Select
        value={this.selectedOption}
        onChange={this.selectHandler}
        options={countriesOptions}
        name={this.props.name}
      />
);
  }
}

class CurrProvider extends Component {
  state = {
    amt: null
  };

  render () {
    return(
      <CurrContext.Provider value={{
        state: this.state,
        onSelectChange: (e) => { 
          this.setState({ amt: e.target.value });
        }
      }}>
        {this.props.children}
      </CurrContext.Provider>
    )
  }
}

class App extends Component {
  render() {
    return (
      <CurrProvider>
        <div className="App">
          <CurrContext.Consumer>
            {(context) => (
              <React.Fragment>
                <input type="text" name="name" className="App-input" onChange={context.onSelectChange} />
                <CountriesList name="fromCurr" />
                <br />
                equals to
                <br />
                <p>{context.state.result}</p>
                <CountriesList name="toCurr" />
              </React.Fragment>
            )}
          </CurrContext.Consumer>
        </div>
      </CurrProvider>
    );
  }
}

export default App;